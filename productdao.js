const fs = require('fs');
const ProductList = require('./productlist.js');

class ProductDao {
    constructor(filePath){
        let fileContent = fs.readFileSync(filePath, 'utf8');
        this.data = JSON.parse(fileContent);
    }

    get allProductsAsArray(){
        return this.data;
    }

    get allAsProductList(){
        return new ProductList(this.data);
    }

}

module.exports = ProductDao;