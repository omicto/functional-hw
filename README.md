## Problemas de programación funcional
>La empresa de abarrotes doña Julia tiene una lista de productos donde almacena la clave del producto, su descripción precio clasificación, cantidad de existencia, existencia mínima y máxima. Doña Julia requiere generar varios reportes:
>
>1) Número de productos con existencia mayor a 20.
>2) Número de productos con existencia menos a 15.
>3) Lista de productos con la misma clasificación y precio mayor 15.50
>4) Lista de productos con precio mayor a 20.30 y menor a 45.00
>5) Número de productos agrupados por su clasificación

## Ejecutando el proyecto
Para ejecutar el proyecto es necesario alimentarlo con un archivo que incluya un arreglo de objetos JSON con informacion sobre los productos.

Digamos que tenemos un archivo llamado `productos.json`
```json
[
    {
        "id": 2,
        "price": 15.3,
        "description":"desodorante",
        "category":"limpieza",
        "stock": 26,
        "minStock":10,
        "maxStock":50
    },
    .
    .
    .
]
```
Luego, simplemente hacemos

```bash
node app.js productos.json
```