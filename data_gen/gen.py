import json
import random

with open('products.json', 'r') as f:
    products_dict = json.load(f)

new_products = []
for product in products_dict:
    stock = random.randint(2,80)
    min_stock = stock - random.randint(0,stock-1)
    max_stock = stock + random.randint(stock, stock * 2)
    my_p = {
        "id":product['filename'].replace('.jpg',''),
        "description": product['title'],
        "price": product['price'],
        "category":product['type'],
        "stock":stock,
        "minStock":min_stock,
        "maxStock":max_stock
    }
    new_products.append(my_p)
    print(product['filename'].replace('.jpg',''))

with open('new_products.json', 'w') as json_file:
    json.dump(new_products, json_file)
