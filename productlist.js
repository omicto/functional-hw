class ProductList{
    constructor(data){
        this.data = data;
    }

    withStockGreaterThan(quantity){
        let newData = this.data.filter(product => product.stock > quantity)
        return new ProductList(newData);
    }
    
    withStockLessThan(quantity){
        let newData = this.data.filter(product => product.stock < quantity);
        return new ProductList(newData);
    }

    withStock(quantity){
        let newData = this.data.filter(product => product.stock === quantity);
        return new ProductList(newData);
    }

    withCategory(category){
        let newData = this.data.filter(product => product.category === category);
        return new ProductList(newData);
    }

    withPrice(price){
        let newData = this.data.filter(product => product.price === price)
        return new ProductList(newData);
    }

    withPriceGreaterThan(price){
        let newData = this.data.filter(product => product.price > price)
        return new ProductList(newData);
    }

    withPriceLessThan(price){
        let newData = this.data.filter(product => product.price < price)
        return new ProductList(newData);
    }

    get number(){
        return this.data.length;
    }


}

module.exports = ProductList;