if(process.argv.length < 3) throw "Please provide a command line arg indicating the source file's name";
const fileName = process.argv[2];
const ProductDao = require('./productdao.js');

let allProducts = new ProductDao(fileName).allAsProductList;

// TODO: Add nice looking output strings
console.log(allProducts.withStockGreaterThan(25));
console.log(allProducts.withStockLessThan(15));
console.log(
    allProducts
    .withCategory("vegetable")
    .withPriceGreaterThan(15.50)
    );

console.log(allProducts
    .withPriceGreaterThan(20.30)
    .withPriceLessThan(45));

console.log(allProducts
    .withCategory("vegetable")
    .number);
